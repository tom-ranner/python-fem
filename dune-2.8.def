Bootstrap: docker
From: debian:stable

%files
    dune-config.opts /opt/dune/config.opts

%post
    echo "Installing required packages..."
    apt-get update
    apt-get install -y \
      mpich libmpich-dev \
      gcc g++ make cmake pkg-config \
      git python3 libpython3-dev python3-venv \
      libalberta-dev

    echo "Downloading dune components"
    mkdir -p /opt/dune
    cd /opt/dune
    git clone --depth=1 --single-branch -b releases/2.8 https://gitlab.dune-project.org/core/dune-common
    git clone --depth=1 --single-branch -b releases/2.8 https://gitlab.dune-project.org/core/dune-geometry
    git clone --depth=1 --single-branch -b releases/2.8 https://gitlab.dune-project.org/core/dune-grid
    git clone --depth=1 --single-branch -b releases/2.8 https://gitlab.dune-project.org/core/dune-istl
    git clone --depth=1 --single-branch -b releases/2.8 https://gitlab.dune-project.org/core/dune-localfunctions
    git clone --depth=1 --single-branch -b releases/2.8 https://gitlab.dune-project.org/dune-fem/dune-fem
    git clone --depth=1 --single-branch -b releases/2.8 https://gitlab.dune-project.org/dune-fem/dune-fempy
    git clone --depth=1 --single-branch -b releases/2.8 https://gitlab.dune-project.org/extensions/dune-alugrid

    # cherry pick fixes
    cd dune-fem
    git fetch origin master
    # [bugfix] use the jacobian range type from the local function in the local function geometry
    git cherry-pick -n -X theirs 6993696a
    # [bugfix] Fix cast to gridEntity when more than one layer of grid part is used
    git cherry-pick -n -X theirs 417364fe2f9d84e14d9ace635919a6f168848f0d
    cd -

    echo "Activating environment"
    export DUNE_ENV_PATH="/opt/dune/dune-env"
    python3 -m venv ${DUNE_ENV_PATH} --system-site-packages
    . ${DUNE_ENV_PATH}/bin/activate
    pip install \
      numpy mpi4py \
      ufl scipy matplotlib \
      meshio jupyter jupyterlab

    echo "Compiling dune"
    ./dune-common/bin/dunecontrol --opts=/opt/dune/config.opts all

    # TODO fix the tests that fail with this set up
    # echo "Testing dune"
    # ./dune-common/bin/dunecontrol bexec make build_tests
    # ./dune-common/bin/dunecontrol bexec ctest -V

    echo "Installing dune"
    ./dune-common/bin/setup-dunepy.py --opts=config.opts install
    ./dune-common/bin/dunecontrol make install

%labels
    Author: Tom Ranner <T.Ranner@leeds.ac.uk>
    Version: v0.1.0

%runscript
    export DUNE_ENV_PATH="/opt/dune/dune-env"
    export MODULEPATH=/usr/share/modulefiles
    . ${DUNE_ENV_PATH}/bin/activate
    jupyter lab --no-browser --ip=127.0.0.1

%help
    This is the singularity build file that I use for running dune (https://dune-project.org).

    To build the image run (note this requires root permissions):
    $ sudo singularity build dune-2.8.sif dune-2.8.def

    The container should be run via the dune-python script which ensures that appropriate temporary directories are created and mapped.

    You can also run the python within the container for example as:
    $ ./dune-2.8.sif --version
    Python 3.9.2

    For more details on singularity see:
    https://sylabs.io/guides/3.7/user-guide/introduction.html
